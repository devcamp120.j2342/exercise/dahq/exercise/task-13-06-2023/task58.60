package com.devcamp.task5810api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Task5810ApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(Task5810ApiApplication.class, args);
	}

}
